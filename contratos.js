Vue.use(window.vuelidate.default)
const { required, minLength } = window.validators;

var appContratos = new Vue({
  el: "#appContratos",
  data: {
    errors: [],
    showModal: false,
    contrato: {
      estado: 0,
      nit: ''
    },
    contratos: [],
    total: 0,
    estados: ['Seleccione...', 'Activo', 'Borrador', 'Cancelado', 'En ejecución', 'Modificado']
  },
  methods: {
    listarContratos: function() {
      axios({
          method: 'get',
          baseURL: 'https://www.datos.gov.co/resource/jbjy-vk9h.json',
        })
        .then(response => {
          let data = response.data;
          // console.log(data);
          let contratos = this.contratos;
          let total = 0;
          let element = {};
          data.map(function(contrato, indice) {
            element = {
              nombre: contrato.nombre_entidad,
              nit: contrato.nit_entidad,
              proceso: contrato.proceso_de_compra,
              id: contrato.id_contrato,
              estado: contrato.estado_contrato
            };
            contratos.push(element);
            total++;
          });
          this.total = total;
        })
        .catch(function(error) {
          if (error.hasOwnProperty('response') && Object.keys(error.response).length > 0) {
            console.log('Estado del error: ' + error.response.status + '. Mensaje: ' + error.response.data.error);
          }
        });
    },
    buscarContratos: function () {
      let frm = '';
      if(this.contrato.estado > 0) {
        frm += 'estado_contrato=' + this.estados[this.contrato.estado];
      }
      if(this.contrato.nit.length > 0) {
        frm += '&nit_entidad=' + this.contrato.nit;
      }
      frm = 
      axios({
        method: 'get',
        baseURL: 'https://www.datos.gov.co/resource/jbjy-vk9h.json?'+ frm,
      })
      .then(response => {
        let data = response.data;
        // console.log(data);
        let contratos = this.contratos;
        let total = 0;
        contratos.splice(0, contratos.length);
        let element = {};
        data.map(function(contrato, indice) {
          element = {
            nombre: contrato.nombre_entidad,
            nit: contrato.nit_entidad,
            proceso: contrato.proceso_de_compra,
            id: contrato.id_contrato,
            estado: contrato.estado_contrato
          };
          contratos.push(element);
          total++;
        });
        this.total = total;
      })
      .catch(function(error) {
        if (error.hasOwnProperty('response') && Object.keys(error.response).length > 0) {
          console.log('Estado del error: ' + error.response.status + '. Mensaje: ' + error.response.data.error);
        }
      });
    },
    resetForm: function() {
      this.contrato = {
        estado: 0,
        nit: ''
      }
      this.listarContratos();
    }
  },
  created: function() {
    this.listarContratos();
  }
});